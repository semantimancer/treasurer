{ mkDerivation, lib, base, directory, filepath, optparse-generic, fuzzy, monomer }:
mkDerivation {
  pname = "treasurer-cli";
  version = "1.0.0.1";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base directory filepath optparse-generic fuzzy monomer
  ];
  license = lib.licenses.gpl3;
}

