# Your Trustworthy Treasurer

Treasurer is a simple finance program for maintaining budgets. The goal is to provide a tool which will
emulate a lot of the by-hand "envelope" method for keeping track of money, while also allowing for
easy electronic record-keeping.

At the moment this is purely a command-line utility accessed via `treasurer-cli` but there are plans
for both TUI and GUI implementations.

## Installing

### Cabal

Use cabal v2 to build & install treasurer.

```
$ cd /path/to/treasurer
$ cabal install time optparse-generic directory filepath
$ cabal v2-build
```

### Nix

Treasurer can be run in a local environment with the `shell.nix` and `default.nix` files.

Using nix-build:

```
$ cd /path/to/treasurer
$ nix-build
```

Using lorri:
```
$ cd /path/to/treasurer
$ direnv allow
```

If you want to call it from a remote .nix file use `treasurer.nix` instead. You can include it in a NixOS configuration with the following:

```
{ config, pkgs, ... }:

let
  treasurer = pkgs.haskellPackages.callPackage /path/to/treasurer/treasurer.nix {};
in {
  environment.systemPackages = with pkgs; [
    treasurer
  ];
}
```

## Usage

You can get a list of all commands with `treasurer-cli --help` and a list of all command options with `treasurer-cli help <command>`.

Treasurer categorizes all items into one of three lists based on how much planning and record-keeping an item needs.

* **Pending:** Any item you expect to be resolved before the next rollover goes here. This includes one-time purchases that you know you'll be making, paychecks, and larger categories of purchases that you're not done spending on yet (e.g. grocery budget).
* **Longterm:** Anything you anticipate that doesn't go into pending goes here. This includes your savings and expenses you know are coming up sometime after the next rollover.
* **Resolved:** Items that are done with and not likely to change are moved here. This lets you easily review them when you need to, and older/unneeded items will be moved out or erased as necessary when you rollover.

Create new items with *add*. You can just run `treasurer-cli add` and answer questions about the item, or you can specify the options yourself with just the one command.

```
$ treasurer-cli add -n <name> -i <item type> -e <expense> -p <profit> -o <notes> -l <list>
```

Every item has to have one of the types outlined below. This doesn't affect any calculations but does change how the items are grouped within a list and can change how profits/expenses are tracked.


* **Required Budget:** A group of related purchases (envelope) that you've budgeted against which you can't avoid.
* **Required Expense:** A one-time purchase which you can't avoid.
* **Elective Budget:** A group of related purchases (envelope) that you've budgeted against which would be nice to have.
* **Elective Expense:** A one-time purchase which would be nice to have.
* **Sinking Fund:** A savings "fund" which represents money you've set aside for something nice to have.
* **Required Fund:** A savings "fund" which represents money you've set aside for something you won't be able to avoid.
* **Profit:** Payment, reimbursement, etc.
* **Balance:** A lump sum of money. Used as a catch-all category or to represent money that isn't specifically set aside for any one thing (e.g. extra cash in a checking or savings account).

Budgets and funds track both current and expected values for profits/expenses. When you make a purchase against a budget (equivalent of taking money out of an envelope to buy something) use *update* to change the current value. You can specify the new value yourself or type `+<amount>` or `-<amount>` to add or subtract that amount from what's already there.

Every item can track both a profit and an expense. Among other uses, this lets you consolidate the purchase itself with any savings you might be using (from the longterm list) that you're using to pay for it. Just add the profit to the purchased item and delete or decrement the savings item that sum was previously connected to.

As you find yourself finished with pending items, use either *close* or *resolve*. Both commands move an item from pending to resolved, but *close* will also set any current profits/expenses to their expected values. You can use *open* and *unresolve*, respectively, to undo those commands (note that *open* will set the current to $0, not its previous values).

If you decide a pending item isn't going to be done before the next rollover you can use *putoff* to move it to longterm instead. Use *anticipate* to move it back to pending when it gets closer to the date.

When you want to check how your finances are doing, use *list* to see a detailed accounting. You can use `-p`, `-r`, and `-l` to specify a list or run it without other arguments to view all three. Or use *current* for a general overview of your running totals.

After the end of your budgeting/billing cycle use *rollover* to start fresh. This clears the pending and resolved lists of any non-recurring items, resets budgets, and unresolves everything except balance items. Note that rollover doesn't touch anything in longterm; the point of that list is to hold things across rollover.

### Data

Treasurer relies on data in your `$XDG_DATA_HOME` directory, typically `~/.local/share` (Unix) or `%APPDATA%` (Windows). If it can't find the necessary files, such as when you run it for the first time, treasurer will create them.

## TODO

* TUI via brick
* GUI via ???
* Expand fuzzy-search from just the show commands to also allow update, resolve, etc
* Configuration files and command-line options to allow for switching between sheet files
* Export sheet files as XML and/or JSON
