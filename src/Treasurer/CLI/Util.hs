{-# LANGUAGE DeriveDataTypeable #-}

module Treasurer.CLI.Util where

import Data.Char
import Data.Maybe
import Data.Time
import qualified Text.Fuzzy as F
import Treasurer
import Treasurer.CLI.Types

typeAbbrToType :: ItemTypeAbbr -> ItemType
typeAbbrToType x = case x of
  RB -> RBudget
  RE -> RExpense
  EB -> EBudget
  EE -> EExpense
  SF -> SFund
  RF -> RFund
  P -> Profit
  _ -> Balance

showItem :: Item -> String
showItem i =
  concat
    [ itemName i
    , " [" ++ (showType $ itemType i)
    , if itemRecurring i then ", Recurring" else ""
    , "] ("
    , if itemCurrentDelta i > 0 then "+" else ""
    , showBreakdown itemCurrentDelta itemExpectedDelta True i
    , showPlusMinus
    , ")"
    , if isJust $ itemDue i
        then "Due: " ++ (showGregorian $ fromJust $ itemDue i)
        else ""
    , if itemNote i /= "" then "\n        " ++ (itemNote i) else ""
    ]
 where
  showProfits = showBreakdown itemCurrentProfit itemExpectedProfit False i
  showExpenses = showBreakdown itemCurrentExpense itemExpectedExpense False i
  showPlusMinus =
    if showProfits /= "0.0" && showExpenses /= "0.0"
      then concat [" <+", showProfits, ", -", showExpenses, ">"]
      else ""

showInspectItem :: Item -> String
showInspectItem i
  | (itemHistory i == []) = showItem i
  | otherwise =
      concat [showItem i, "\nHistory:", concatMap showHistory $ itemHistory i]
 where
  showHistory (d, e, p) = concat ["\n", showGregorian d, showProfit p, showExpense e, showTotal e p]

  showExpense e = "   Expense: " ++ (show e)
  showProfit p = "   Profit: +" ++ (show p)
  showTotal e p =
    if (p - e) > 0.0
      then "   Total: +" ++ (show (p - e))
      else "   Total: " ++ (show (p - e))

fzfShow :: String -> [SheetList] -> Sheet -> String
fzfShow q ls sheet = if accumulated == "" then "Nothing found!" else accumulated
 where
  accumulated = foldl acc "" ls
  acc str l =
    let x = fzfShow' q l sheet
     in if null x
          then str
          else concatMap (++ "\n") $ [str, map toUpper $ show l] ++ x

fzfShow' :: String -> SheetList -> Sheet -> [String]
fzfShow' q l sheet = map (showItem . snd) $ fzfInItems q $ (sheetList l) sheet

fzfInItems :: String -> [Item] -> [(Int, Item)]
fzfInItems q is = map (\x -> (F.score x, F.original x)) $ F.filter q is "" "" showItem False

-- See CLI's addItem for how these are collected
newItemFromArgs
  :: String -> ItemTypeAbbr -> Float -> Float -> String -> Bool -> Item
newItemFromArgs iName iType iExpense iProfit iNote iRecur =
  newItem
    iName
    (typeAbbrToType iType)
    iExpense
    iProfit
    (if iNote == "N" then "" else iNote)
    iRecur

-- "static" lists are those that typically won't have much movement, where you don't need to
-- track the current vs expected as finely
displayStaticList :: String -> [Item] -> String
displayStaticList name is =
  displayList
    name
    (show' itemCurrentDelta)
    (show' itemCurrentProfit)
    (show' ((*) (-1) . itemCurrentExpense))
    is
 where
  show' f = show . sum $ map f is

-- "dynamic" lists are those where you want to keep good track of current vs expected
displayDynamicList :: String -> [Item] -> String
displayDynamicList name is =
  displayList
    name
    (show' itemCurrentDelta itemExpectedDelta)
    (show' itemCurrentProfit itemExpectedProfit)
    (show' ((*) (-1) . itemCurrentExpense) ((*) (-1) . itemExpectedExpense))
    is
 where
  show' current expected =
    showBreakdown
      (sum . map current)
      (sum . map expected)
      False
      is

displayList :: String -> String -> String -> String -> [Item] -> String
displayList name delta profits expenses is =
  concat $
    [ "\n"
    , name
    , "\n  Total Delta: "
    , delta
    , if profits /= "0.0" then "\n    - Profits: " ++ profits else ""
    , if expenses /= "0.0" then "\n    - Expenses: " ++ expenses else ""
    , "\n"
    ]
      ++ (displayRows is)

displayRows :: [Item] -> [String]
displayRows = zipWith row ([1 ..] :: [Int])
 where
  row n i =
    let f = if n < 10 then (:) '0' else id
     in concat ["\n  ", f $ show n, ". ", showItem i]

showBreakdown :: (a -> Float) -> (a -> Float) -> Bool -> a -> String
showBreakdown current expected percentage item
  | (current item) == 0.0 = show $ expected item
  | (expected item) == 0.0 = show $ current item
  | (current item) == (expected item) = show $ expected item
  | otherwise =
      concat
        [ show $ current item
        , "/"
        , show $ expected item
        , if percentage
            then ", " ++ (show $ ceiling' $ ((current item) / (expected item) * 100)) ++ "%"
            else ""
        ]
 where
  -- Purely doing this to suppress a warning
  ceiling' :: Float -> Int
  ceiling' = ceiling
