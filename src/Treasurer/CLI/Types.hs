module Treasurer.CLI.Types where

data ItemTypeAbbr = RB | RE | EB | EE | SF | RF | P | B
  deriving (Read, Show)

-- ItemTypeAbbr has a ParseField instance in app/CLI/Main.hs
