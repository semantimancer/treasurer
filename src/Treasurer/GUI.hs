{-# LANGUAGE OverloadedStrings #-}

module Treasurer.GUI where

import Monomer

import qualified Data.Text as T

import Treasurer
import Treasurer.GUI.Types

itemList :: T.Text -> [Item] -> YTTNode
itemList txt is =
  (flip styleBasic)
    [padding 3, border 10 transparent]
    $ vstack
    $ [ spacer
      , label txt `styleBasic` [textSize 24.0]
      , spacer
      , vscroll (vstack $ map itemNode is) `styleBasic` [paddingL 5]
      , filler
      , hstack
          [ filler
          , labelS (sum $ map itemCurrentDelta is)
              `styleBasic` [textColor darkGray]
          , spacer
          , labelS (sum $ map itemExpectedDelta is)
          , spacer
          ]
      ]

itemNode :: Item -> YTTNode
itemNode i =
  (flip styleBasic)
    [paddingB 2]
    $ box
    $ vstack
      [ hstack [label' $ itemName i]
      , hstack
          [ spacer
          , itemTypeBox (itemType i)
          , filler
          , widgetIf showExpected $
              labelS (itemCurrentDelta i)
                `styleBasic` [textColor darkGray]
          , spacer
          , labelS $
              if itemStatic i
                then itemCurrentDelta i
                else itemExpectedDelta i
          ]
      ]
      `styleBasic` [border 1 darkGray, padding 5, radius 5.0]
 where
  label' = label . T.pack
  showExpected =
    and
      [ itemCurrentDelta i /= itemExpectedDelta i
      , not $ itemStatic i
      ]

itemTypeBox :: ItemType -> YTTNode
itemTypeBox t =
  label (T.pack $ showType t)
    `styleBasic` [textSize 11, bgColor col, radius 4.0, padding 2]
 where
  col = case t of
    RBudget -> darkRed
    RExpense -> darkRed
    RFund -> darkRed
    EBudget -> darkGoldenRod
    EExpense -> darkGoldenRod
    SFund -> darkGoldenRod
    Profit -> darkGreen
    Balance -> darkGreen
