{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Treasurer.CLI where

import Data.Char
import Data.List
import Data.Maybe
import Data.Time
import System.Directory
import System.FilePath
import Text.Read

import Treasurer
import Treasurer.CLI.Types
import Treasurer.CLI.Util
import Treasurer.IO

withSheet :: (FilePath -> Sheet -> IO ()) -> FilePath -> IO ()
withSheet f dataPath = do
  sheet <- readSheetMB dataPath
  case sheet of
    Nothing -> putStrLn $ "ERROR: Invalid file " ++ (dataPath </> "sheet.treasurer")
    Just s -> f dataPath s

addItemCLI
  :: Maybe String
  -> Maybe ItemTypeAbbr
  -> Maybe Float
  -> Maybe Float
  -> Maybe Bool
  -> Maybe String
  -> Maybe SheetList
  -> FilePath
  -> Sheet
  -> IO ()
addItemCLI n' t' e' p' r' o' l' dataPath sheet = do
  (n : o : _) <-
    mapM
      ( \(fromArg, prompt, check) -> case fromArg of
          Just x -> return x
          Nothing -> queryUser prompt check
      )
      $ zip3 [n', o'] prompts checks
  (e : p : _) <-
    mapM
      ( \(fromArg, prompt) -> case fromArg of
          Just x -> return $ show x
          Nothing -> queryUser prompt (\x -> x == "" || checkFloat x)
      )
      $ zip [e', p'] ["Expense? [0]", "Profit? [0]"]
  t <- case t' of
    Just x -> return x
    _ -> fmap read $ queryUser typePrompt checkItemTypeAbbr
  r <- case r' of
    Just x -> return x
    _ ->
      fmap (`elem` ["y", "Y"]) $
        queryUser "Recurring? [y/N]" (`elem` ["", "y", "Y", "n", "N"])
  l <- case l' of
    Just x -> return x
    _ -> fmap read $ queryUser "List?" checkSheetList

  -- Expenses/profits get defaulted twice because the first default skips the check if CLI args
  -- are provided, this one allows a "" (Enter/null) response to be valid.

  let newItem = newItemFromArgs n t (readDefault e 0.0) (readDefault p 0.0) o r
  addItem newItem l dataPath sheet
 where
  prompts = ["Item Name?", "Notes?", "List?"]
  checks =
    [ (\x -> (length x) > 0)
    , (\_ -> True)
    , (isJust . readSheetListMaybe)
    ]

interactWithItem
  :: (SheetList -> Int -> FilePath -> Sheet -> IO ())
  -> Maybe SheetList
  -> Maybe Int
  -> FilePath
  -> Sheet
  -> IO ()
interactWithItem f (Just list) (Just i) dataPath sheet
  | i > 0
  , i <= (length $ sheetList list sheet) =
      f list i dataPath sheet
  | otherwise = putStrLn $ "ERROR: '" ++ (show i) ++ "' not a valid item number."
interactWithItem f (Just list) Nothing dataPath _ = do
  displaySheet (Just [list]) dataPath
  putStrLn "\nWhich item?"
  input <- getLine
  withSheet (interactWithItem f (Just list) (readMaybe input)) dataPath
interactWithItem f _ _ dataPath sheet = do
  list <- queryUser "Which list?" (isJust . readSheetListMaybe)
  interactWithItem f (readSheetListMaybe list) Nothing dataPath sheet

removeItemCLI :: Maybe SheetList -> Maybe Int -> FilePath -> Sheet -> IO ()
removeItemCLI l n dataPath s = interactWithItem removeItem l n dataPath s

updateItem :: Maybe SheetList -> Maybe Int -> FilePath -> Sheet -> IO ()
updateItem l n dataPath s = interactWithItem updateItem' l n dataPath s

updateItem' :: SheetList -> Int -> FilePath -> Sheet -> IO ()
updateItem' list n dataPath sheet = do
  putStrLn $ showItem oldItem
  opt <-
    if singular $ itemType oldItem
      then
        queryUser
          "Set which field?\n  1. Name\n  2. Type\n  3. Expense\n  4. Profit\n  6. Note\n"
          (`elem` ["1", "2", "3", "4", "6"]) -- TODO: 5
      else
        queryUser
          ( concat
              [ "Set which field?\n"
              , "  1. Name\n"
              , "  2. Type\n"
              , "  3. Current Expense\n"
              , "  4. Expected Expense\n"
              , "  5. Current Profit\n"
              , "  6. Expected Profit\n"
              , "  8. Note\n"
              ]
          )
          (`elem` ["1", "2", "3", "4", "5", "6", "8"]) -- TODO: 7
  ans <-
    if singular $ itemType oldItem
      then uncurry queryUser $ case opt of
        "1" -> ("New Name?", \_ -> True)
        "2" -> (typePrompt, checkItemTypeAbbr)
        "3" -> ("New Expense?", checkFloatMod)
        "4" -> ("New Profit?", checkFloatMod)
        _ -> ("New Note?", \_ -> True)
      else uncurry queryUser $ case opt of
        "1" -> ("New Name?", \_ -> True)
        "2" -> (typePrompt, checkItemTypeAbbr)
        "3" -> ("New (Current) Expense?", checkFloatMod)
        "4" -> ("New (Expected) Expense?", checkFloatMod)
        "5" -> ("New (Current) Profit?", checkFloatMod)
        "6" -> ("New (Expected) Profit?", checkFloatMod)
        _ -> ("New Note?", \_ -> True)

  writeFile (dataPath </> "sheet.treasurer") $
    show $
      actOnSheetList
        sheet
        list
        (\is -> sort $ (newItem opt oldItem ans) : (delete oldItem is))
 where
  applyMod new old = case head new of
    '+' -> old + (read $ tail new)
    '-' -> old - (read $ tail new)
    _ -> read new

  oldItem = (sheetList list sheet) !! (n - 1)
  newItem option item newVal =
    if singular $ itemType oldItem
      then case option of
        "1" -> item {itemName = newVal}
        "2" -> item {itemType = typeAbbrToType $ read newVal}
        "3" ->
          let n = applyMod newVal (itemCurrentExpense item)
           in item {itemCurrentExpense = n, itemExpectedExpense = n}
        "4" ->
          let n = applyMod newVal (itemCurrentProfit item)
           in item {itemCurrentProfit = n, itemExpectedProfit = n}
        _ -> item {itemNote = newVal}
      else case option of
        "1" -> item {itemName = newVal}
        "2" -> item {itemType = typeAbbrToType $ read newVal}
        "3" -> item {itemCurrentExpense = applyMod newVal (itemCurrentExpense item)}
        "4" -> item {itemExpectedExpense = applyMod newVal (itemExpectedExpense item)}
        "5" -> item {itemCurrentProfit = applyMod newVal (itemCurrentProfit item)}
        "6" -> item {itemExpectedProfit = applyMod newVal (itemExpectedProfit item)}
        _ -> item {itemNote = newVal}

inspectItem :: Maybe SheetList -> Maybe Int -> FilePath -> Sheet -> IO ()
inspectItem l n dataPath s = interactWithItem inspectItem' l n dataPath s

inspectItem' :: SheetList -> Int -> FilePath -> Sheet -> IO ()
inspectItem' list n _ sheet = putStrLn $ showInspectItem $ (sheetList list sheet) !! (n - 1)

resolveItem :: Maybe Int -> FilePath -> Sheet -> IO ()
resolveItem n d s = moveItem n pending resolve Pending "Resolve which item?" d s

closeItem :: Maybe Int -> FilePath -> Sheet -> IO ()
closeItem n d s = moveItem n pending close Pending "Close which item?" d s

unresolveItem :: Maybe Int -> FilePath -> Sheet -> IO ()
unresolveItem n d s = moveItem n resolved unresolve Resolved "Unresolve which item?" d s

openItem :: Maybe Int -> FilePath -> Sheet -> IO ()
openItem n d s = moveItem n resolved open Resolved "Open which item?" d s

putoffItem :: Maybe Int -> FilePath -> Sheet -> IO ()
putoffItem n d s = moveItem n pending putoff Pending "Put off which item?" d s

anticipateItem :: Maybe Int -> FilePath -> Sheet -> IO ()
anticipateItem n d s = moveItem n longterm anticipate LongTerm "Anticipate which item?" d s

moveItem
  :: Maybe Int
  -> (Sheet -> [Item]) -- Function to pull the correct list out of the sheet
  -> (Sheet -> Item -> Sheet) -- Function to apply
  -> SheetList -- List to use with display
  -> String -- Question to prompt user
  -> FilePath
  -> Sheet
  -> IO ()
moveItem (Just i) getList apply _ _ dataPath sheet
  | i > 0
  , i <= (length $ getList sheet) =
      writeFile
        (dataPath </> "sheet.treasurer")
        (show $ apply sheet ((getList sheet) !! (i - 1)))
  | otherwise = putStrLn $ "ERROR: '" ++ (show i) ++ "' not a valid item number."
moveItem Nothing getList apply list prompt dataPath _ = do
  displaySheet (Just [list]) dataPath
  putStrLn $ "\n" ++ prompt
  input <- getLine
  withSheet (moveItem (readMaybe input) getList apply list prompt) dataPath

-- Todo: get rid of FilePath arg?
showInSheet :: Maybe SheetList -> String -> FilePath -> Sheet -> IO ()
showInSheet (Just l) q _ sheet = putStrLn $ fzfShow q [l] sheet
showInSheet Nothing q _ sheet = putStrLn $ fzfShow q [Pending, Resolved, LongTerm] sheet

-- showInSheet (Just x) q dataPath sheet = mapM_ putStrLn $ map (showItem . snd) $ fzfInItems q $ (sheetList x) sheet
-- showInSheet Nothing q dataPath sheet = mapM_ (\x -> showSheetGroup x) [Pending,Resolved,LongTerm]
--  where showSheetGroup x = do
--          putStrLn $ "***" ++ (map toUpper $ show x)
--          showInSheet (Just x) q dataPath sheet
--          putStrLn "\n"

displayCurrent :: FilePath -> IO ()
displayCurrent dataPath = do
  sheet <-
    (fmap readMaybe) . readFile $ dataPath </> "sheet.treasurer" :: IO (Maybe Sheet)
  putStrLn $ case sheet of
    Nothing -> "ERROR: Invalid file " ++ (dataPath </> "sheet.treasurer")
    Just s ->
      "Current Balances\n  Short-Term: "
        ++ (show $ realDelta [Resolved, Pending] s)
        ++ "\n  Long-Term:  "
        ++ (show $ realDelta [LongTerm] s)

displaySheet :: Maybe [SheetList] -> FilePath -> IO ()
displaySheet (Just []) _ = return ()
displaySheet (Just xs) dataPath = mapM_ (\x -> displaySheet' dataPath (sheetList x) (map toUpper $ show x)) xs
displaySheet Nothing dataPath = displaySheet (Just [Pending, Resolved, LongTerm]) dataPath

displaySheet' :: FilePath -> (Sheet -> [Item]) -> String -> IO ()
displaySheet' dataPath f name = do
  sheet <-
    (fmap readMaybe) . readFile $ dataPath </> "sheet.treasurer" :: IO (Maybe Sheet)
  putStrLn $ case sheet of
    Nothing -> "ERROR: Invalid file " ++ (dataPath </> "sheet.treasurer")
    Just s ->
      if name == "RESOLVED"
        then displayStaticList name $ f s
        else displayDynamicList name $ f s

rolloverSheet :: FilePath -> Sheet -> IO ()
rolloverSheet dataPath sheet = do
  copyFile (dataPath </> "sheet.treasurer") (dataPath </> "backup.treasurer")
  writeFile (dataPath </> "sheet.treasurer")
    =<< fmap (show . rollover sheet . utctDay) getCurrentTime
  putStrLn $ "Backup saved to: " ++ (dataPath </> "backup.treasurer")
  displaySheet Nothing dataPath

queryUser :: String -> (String -> Bool) -> IO String
queryUser str check = do
  putStrLn str
  input <- getLine
  if check input
    then return input
    else putStrLn "Invalid input, please try again." >> queryUser str check

checkFloat :: String -> Bool
checkFloat s = let x = readMaybe s :: Maybe Float in isJust x

checkFloatMod :: String -> Bool
checkFloatMod x = if head x `elem` ['+', '-'] then checkFloat (tail x) else checkFloat x

checkItemTypeAbbr :: String -> Bool
checkItemTypeAbbr s = let x = readMaybe s :: Maybe ItemTypeAbbr in isJust x

checkSheetList :: String -> Bool
checkSheetList s = let x = readMaybe s :: Maybe SheetList in isJust x

typePrompt :: String
typePrompt =
  concat
    [ "Type?\n"
    , "  RB: Required Budget\n"
    , "  RE: Required Expense\n"
    , "  EB: Elective Budget\n"
    , "  EE: Elective Expense\n"
    , "  SF: Sinking Fund\n"
    , "  RF: Required Fund\n"
    , "  P:  Profit\n"
    , "  B. Balance\n"
    ]
