{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Treasurer.GUI.Types where

import Control.Lens (makeLenses)
import Data.Text (Text)
import Monomer

import Treasurer

type YTTWenv = WidgetEnv YTTModel YTTEvent

type YTTNode = WidgetNode YTTModel YTTEvent

type YTTResp = AppEventResponse YTTModel YTTEvent

data YTTModel = YTTModel
  {_modelSheet :: Sheet}
  deriving (Eq, Show)

data YTTEvent
  = EvNull

makeLenses 'YTTModel
