module Treasurer.IO where

import Control.Monad (when)
import Data.List
import System.Directory
import System.FilePath ((</>))
import Text.Read (readMaybe)

import Treasurer

-- TODO: Configuration to allow for other sheets
--        Probably create a Config type and pass dataPath, file, etc in that
-- TODO: Store sheets as CSV files
establishDataDir :: IO FilePath
establishDataDir = do
  dataPath <- getXdgDirectory XdgData "treasurer"
  let defFile = dataPath </> "sheet.treasurer"

  createDirectoryIfMissing True dataPath
  checkFile <- doesFileExist defFile
  when (not checkFile) $ writeFile defFile "([],[],[])"

  return dataPath

readSheetMB :: FilePath -> IO (Maybe Sheet)
readSheetMB dataPath = (fmap readMaybe) . readFile $ dataPath </> "sheet.treasurer"

addItem :: Item -> SheetList -> FilePath -> Sheet -> IO ()
addItem item list dataPath sheet =
  writeSheet dataPath $ actOnSheetList sheet list (\is -> sort $ item : is)

removeItem :: SheetList -> Int -> FilePath -> Sheet -> IO ()
removeItem list n dataPath sheet =
  writeSheet dataPath $ actOnSheetList sheet list (\is -> (take (n - 1) is) ++ (drop n is))

writeSheet :: FilePath -> Sheet -> IO ()
writeSheet dataPath sheet = writeFile (dataPath </> "sheet.treasurer") (show sheet)
