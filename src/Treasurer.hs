module Treasurer where

import Data.Char
import Data.List
import Data.Time
import Text.Read

data ItemType
  = RBudget
  | RExpense
  | EBudget
  | EExpense
  | Profit
  | SFund
  | RFund
  | Balance
  deriving (Read, Show, Eq, Ord)

showType :: ItemType -> String
showType RBudget = "Required Budget"
showType RExpense = "Required Expense"
showType EBudget = "Elective Budget"
showType EExpense = "Elective Expense"
showType Profit = "Profit"
showType SFund = "Sinking Fund"
showType RFund = "Required Fund"
showType Balance = "Balance"

singular :: ItemType -> Bool
singular = (`elem` [RExpense, EExpense, Profit, Balance])

-- (Date,Expense on that date,Profit on that date)
type History = [(Day, Float, Float)]

data Item = Item
  { itemName :: String
  , itemType :: ItemType
  , itemCurrentExpense :: Float
  , itemExpectedExpense :: Float
  , itemCurrentProfit :: Float
  , itemExpectedProfit :: Float
  , itemRecurring :: Bool
  , itemDue :: Maybe Day
  , itemNote :: String
  , itemHistory :: History
  }
  deriving (Read, Show, Eq)

instance Ord Item where
  compare i1 i2
    | (itemType i1) /= (itemType i2) = compare (itemType i1) (itemType i2)
    | (itemExpectedDelta i1) /= (itemExpectedDelta i2) =
        compare
          (itemExpectedDelta i1)
          (itemExpectedDelta i2)
    | (itemCurrentDelta i1) /= (itemCurrentDelta i2) =
        compare
          (itemCurrentDelta i1)
          (itemCurrentDelta i2)
    | (itemDue i1) /= (itemDue i2) = compare (itemDue i1) (itemDue i2)
    | otherwise = compare (itemName i1) (itemName i2)

itemCurrentDelta :: Item -> Float
itemCurrentDelta i = (itemCurrentProfit i) - (itemCurrentExpense i)

itemExpectedDelta :: Item -> Float
itemExpectedDelta i = (itemExpectedProfit i) - (itemExpectedExpense i)

itemStatic :: Item -> Bool
itemStatic i = itemType i `elem` [SFund, RFund, Balance]

itemStartsAtZero :: Item -> Bool
itemStartsAtZero = not . singular . itemType

itemOpen :: Item -> Item
itemOpen i = i {itemCurrentExpense = 0, itemCurrentProfit = 0}

itemClose :: Item -> Item
itemClose i =
  i
    { itemCurrentExpense = itemExpectedExpense i
    , itemCurrentProfit = itemExpectedProfit i
    }

type Sheet = ([Item], [Item], [Item])

pending, resolved, longterm :: Sheet -> [Item]
pending (p, _, _) = p
resolved (_, r, _) = r
longterm (_, _, l) = l

resolve, close, unresolve, open, putoff, anticipate :: Sheet -> Item -> Sheet
resolve (p, r, l) i = (delete i p, sort $ i : r, l)
close (p, r, l) i = (delete i p, sort $ (itemClose i) : r, l)
unresolve (p, r, l) i = (sort $ i : p, delete i r, l)
open (p, r, l) i = (sort $ (itemOpen i) : p, delete i r, l)
putoff (p, r, l) i = (delete i p, r, sort $ i : l)
anticipate (p, r, l) i = (sort $ i : p, r, delete i l)

data SheetList = Pending | Resolved | LongTerm
  deriving (Eq)

-- This probably isn't terribly great practice but I'm pretty sure it still maintains
-- all the usual rules about how Read & Show should work (e.g. Read -> Show -> Read)
instance Read SheetList where
  readsPrec _ x = case map toLower x of
    "pending" -> [(Pending, "")]
    "resolved" -> [(Resolved, "")]
    "longterm" -> [(LongTerm, "")]
    "long" -> [(LongTerm, "")]
    "long-term" -> [(LongTerm, "")]
    "r" -> [(Resolved, "")]
    "p" -> [(Pending, "")]
    "l" -> [(LongTerm, "")]
    _ -> []

instance Show SheetList where
  show Pending = "pending"
  show Resolved = "resolved"
  show LongTerm = "longterm"

-- ParseField instance in CLI/Main

sheetList :: SheetList -> Sheet -> [Item]
sheetList Pending = pending
sheetList Resolved = resolved
sheetList LongTerm = longterm

actOnSheetList :: Sheet -> SheetList -> ([Item] -> [Item]) -> Sheet
actOnSheetList (p, r, l) Pending f = (f p, r, l)
actOnSheetList (p, r, l) Resolved f = (p, f r, l)
actOnSheetList (p, r, l) LongTerm f = (p, r, f l)

realDelta :: [SheetList] -> Sheet -> Float
realDelta [] _ = 0
realDelta xs sheet = (total itemCurrentProfit) - (total itemCurrentExpense)
 where
  total f = sum . map f $ concatMap (\x -> sheetList x sheet) xs

rollover :: Sheet -> Day -> Sheet
rollover (p, r, l) day = (sort $ map reset $ keepP ++ rollingR, staticR, map updateHist l)
 where
  makeHist i = (day, itemCurrentExpense i, itemCurrentProfit i)
  reset i =
    i
      { itemCurrentExpense = if itemStartsAtZero i then 0 else itemExpectedExpense i
      , itemCurrentProfit = if itemStartsAtZero i then 0 else itemExpectedProfit i
      , itemHistory = (makeHist i) : (itemHistory i)
      }

  rollingR = filter itemRecurring r
  keepP = filter (\x -> itemRecurring x || itemStatic x) p

  staticR = filter itemStatic r

  updateHist i = if itemStatic i then i {itemHistory = (makeHist i) : (itemHistory i)} else i

actual :: (Num a, Ord a) => a -> a -> a
actual a b = if (abs a) > (abs b) then a else b

readDefault :: (Read a) => String -> a -> a
readDefault s def = case readMaybe s of
  Nothing -> def
  Just a -> a

readSheetListMaybe :: String -> Maybe SheetList
readSheetListMaybe = readMaybe

newItem :: String -> ItemType -> Float -> Float -> String -> Bool -> Item
newItem iName iType iExpense iProfit iNote iRecur =
  Item
    { itemName = iName
    , itemType = iType
    , itemCurrentExpense = if singular iType then iExpense else 0
    , itemExpectedExpense = iExpense
    , itemCurrentProfit = if singular iType then iProfit else 0
    , itemExpectedProfit = iProfit
    , itemRecurring = iRecur
    , itemDue = Nothing -- TODO
    , itemNote = iNote
    , itemHistory = []
    }
