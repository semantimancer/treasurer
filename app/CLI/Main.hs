{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad
import Data.Char
import Data.List
import Data.Maybe
import Data.Time
import Options.Generic
import System.Directory
import System.Environment
import System.FilePath
import Text.Read
import Treasurer
import Treasurer.CLI
import Treasurer.CLI.Types
import Treasurer.IO

data Command
  = Add
      { name :: Maybe String <#> "n" <?> "Can include spaces if surrounded with \"s."
      , itemtype :: Maybe ItemTypeAbbr <#> "i" <?> "One of RB, RE, EB, EE, SF, RF, P, or B."
      , expense :: Maybe Float <#> "e"
      , profit :: Maybe Float <#> "p"
      , recurring :: Bool <#> "r" <?> "Defaults to non-recurring unless specified"
      , notes :: Maybe String <#> "o" <?> "Can include spaces if surrounded with \"s."
      , list :: Maybe SheetList <#> "l" <?> "One of [p]ending, [r]esolved, or [l]ongterm."
      }
  | Remove
      { list :: Maybe SheetList <#> "l" <?> "One of [p]ending, [r]esolved, or [l]ongterm."
      , item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."
      }
  | Update
      { list :: Maybe SheetList <#> "l" <?> "One of [p]ending, [r]esolved, or [l]ongterm."
      , item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."
      }
  | Inspect
      { list :: Maybe SheetList <#> "l" <?> "One of [p]ending, [r]esolved, or [l]ongterm."
      , item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."
      }
  | Resolve
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Close
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Unresolve
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Open
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Putoff
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Anticipate
      {item :: Maybe Int <#> "i" <?> "Number in list. Use LIST or leave blank if unknown."}
  | Current
  | Rollover
  | Show String
  | ShowIn (Maybe SheetList) String
  | List {p :: Bool <#> "p", r :: Bool <#> "r", l :: Bool <#> "l"}
  deriving (Generic, Show)

instance ParseRecord Command

instance ParseField ItemTypeAbbr where
  metavar _ = "ITEM_TYPE_ABBR"

instance ParseField SheetList where
  metavar _ = "LIST_CHOICE"

main :: IO ()
main = run =<< getRecord "Your Trustworthy Treasurer (treasurer-cli)"

run :: Command -> IO ()
run c = f c =<< establishDataDir
 where
  f (Add n i e p r o l) =
    let f = unShortName . unHelpful
        -- See comment in addItem for why this def function is here
        def x param = if isJust param then param else Just x
     in case (f n, f i, unShortName e, unShortName p, f r, f o, f l) of
          (Nothing, Nothing, Nothing, Nothing, False, Nothing, Nothing) ->
            withSheet $ addItemCLI Nothing Nothing Nothing Nothing Nothing Nothing Nothing
          (n', i', e', p', r', o', l') ->
            withSheet $ addItemCLI n' i' (def 0.0 e') (def 0.0 p') (Just r') (def "" o') l'
  f (Remove l n) = withSheet (removeItemCLI (getDeconstruct l) (getDeconstruct n))
  f (Update l n) = withSheet (updateItem (getDeconstruct l) (getDeconstruct n))
  f (Inspect l n) = withSheet (inspectItem (getDeconstruct l) (getDeconstruct n))
  f (Resolve n) = withSheet (resolveItem $ getDeconstruct n)
  f (Close n) = withSheet (closeItem $ getDeconstruct n)
  f (Unresolve n) = withSheet (unresolveItem $ getDeconstruct n)
  f (Open n) = withSheet (openItem $ getDeconstruct n)
  f (Putoff n) = withSheet (putoffItem $ getDeconstruct n)
  f (Anticipate n) = withSheet (anticipateItem $ getDeconstruct n)
  f Current = displayCurrent
  f Rollover = withSheet rolloverSheet
  f (Show q) = withSheet (showInSheet Nothing q)
  f (ShowIn l q) = withSheet (showInSheet l q)
  f (List (ShortName False) (ShortName False) (ShortName False)) = displaySheet Nothing
  f (List (ShortName p) (ShortName r) (ShortName l)) =
    displaySheet $
      Just $
        map snd $
          filter fst $
            zip [p, r, l] [Pending, Resolved, LongTerm]

  getDeconstruct x = unShortName $ unHelpful x
