{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Lens
import Monomer
import System.Directory
import System.FilePath

import qualified Data.Text as T

import Treasurer
import Treasurer.GUI
import Treasurer.GUI.Types
import Treasurer.IO

main :: IO ()
main = do
  dataPath <- establishDataDir
  model <- establishModel dataPath
  startApp model handleEvent buildUI (monomerConfig dataPath)

establishModel :: FilePath -> IO YTTModel
establishModel dataPath = do
  sheet <- readSheetMB dataPath
  return $ case sheet of
    Nothing -> YTTModel ([], [], [])
    Just x -> YTTModel x

monomerConfig :: FilePath -> [AppConfig s YTTEvent]
monomerConfig dataPath =
  [ appWindowTitle "Your Trustworthy Treasurer"
  , appTheme darkTheme
  , appFontDef "Regular" (T.pack $ dataPath </> "Roboto-Regular.ttf")
  , appInitEvent EvNull
  , appDisableAutoScale True
  ]

buildUI :: YTTWenv -> YTTModel -> YTTNode
buildUI wenv model =
  hgrid
    [ itemList "Pending" (pending $ model ^. modelSheet)
    , itemList "Resolved" (resolved $ model ^. modelSheet)
    , itemList "Longterm" (longterm $ model ^. modelSheet)
    ]
    `styleBasic` [padding 3]

handleEvent :: YTTWenv -> YTTNode -> YTTModel -> YTTEvent -> [YTTResp]
handleEvent wenv _ model evt = case evt of
  EvNull -> []
